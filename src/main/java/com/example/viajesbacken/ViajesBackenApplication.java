package com.example.viajesbacken;

import com.example.viajesbacken.converters.SingleModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class ViajesBackenApplication {

    @Value("${trip.property.crossOrigin}")
    private String crossOrigin;

    public static void main(String[] args) {
        SpringApplication.run(ViajesBackenApplication.class, args);
    }

    @Bean
    public SingleModelMapper singleModelMapper() {
        return new SingleModelMapper();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {

        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins(crossOrigin).allowedMethods("*").allowedHeaders("*");
            }
        };
    }

}
