package com.example.viajesbacken.constants;

/**
 * Clase de constantes con los nombres de los métodos de controladores, para agregar errores personalizados
 */
public class ErrorMessageConstants {

    public static final String GET_ALL = "METHOD getAll() HAS ERROR: ";

    public static final String GET_ALL_BY_CITY = "METHOD getAllByCity() HAS ERROR: ";

    public static final String GET_ALL_BY_TOURIST = "METHOD getAllByTourist() HAS ERROR: ";

    public static final String GET_BY_ID = "METHOD getById() HAS ERROR: ";

    public static final String SAVE = "METHOD save() HAS ERROR: ";

    public static final String UPDATE = "METHOD update() HAS ERROR: ";

    public static final String DELETE = "METHOD getAll() HAS ERROR: ";

    private ErrorMessageConstants() {

    }
}
