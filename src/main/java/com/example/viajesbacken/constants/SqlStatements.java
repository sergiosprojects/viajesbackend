package com.example.viajesbacken.constants;

/**
 * Clase de constantes para las sentencias sql que se usan a menudo en las pruebas.
 */
public class SqlStatements {

    public static final String SELECT_ALL_TOURISTS = "SELECT * FROM tourists";

    public static final String SELECT_ALL_TOURISTS_BY_ID = "SELECT * FROM tourists WHERE id=?";

    public static final String COUNT_ALL_TOURISTS_BY_ID = "SELECT COUNT(*) FROM tourists WHERE id=?";

    public static final String SELECT_ALL_CITIES = "SELECT * FROM cities";

    public static final String SELECT_ALL_CITIES_BY_ID = "SELECT * FROM cities WHERE id=?";

    public static final String SELECT_ALL_CITIES_BY_NAME = "SELECT * FROM cities WHERE name=?";

    public static final String COUNT_ALL_CITIES_BY_ID = "SELECT COUNT(*) FROM cities WHERE id=?";

    private SqlStatements() {

    }
}
