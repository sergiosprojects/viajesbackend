package com.example.viajesbacken.converters;

import com.example.viajesbacken.dto.CityDTO;
import com.example.viajesbacken.dto.TouristDTO;
import com.example.viajesbacken.dto.TripDTO;
import com.example.viajesbacken.entity.City;
import com.example.viajesbacken.entity.Tourist;
import com.example.viajesbacken.entity.Trip;
import org.modelmapper.ModelMapper;

/**
 * Se encarga de realizar el mapeo respectivo entre Entities y DTos usando la librería de ModelMapper.
 */

public class SingleModelMapper {

    private final ModelMapper mapper = new ModelMapper();

    /**
     * Mapear una entidad City a partir de un DTO
     * @param cityDTO El tipo de DTO desde el cual se realiza el mapeo
     * @return Nueva entidad de tipo City
     */
    public City mapToCity(CityDTO cityDTO) {
        return mapper.map(cityDTO, City.class);
    }
    /**
     * Mapear una entidad Tourist a partir de un DTO
     * @param touristDTO El tipo de DTO desde el cual se realiza el mapeo
     * @return Nueva entidad de tipo Tourist
     */
    public Tourist mapToTourist(TouristDTO touristDTO) {
        return mapper.map(touristDTO, Tourist.class);
    }
    /**
     * Mapear una entidad Trip a partir de un DTO
     * @param tripDTO El tipo de DTO desde el cual se realiza el mapeo
     * @return Nueva entidad de tipo Trip
     */
    public Trip mapToTrip(TripDTO tripDTO) {
        return mapper.map(tripDTO, Trip.class);
    }
    /**
     * Mapear un CityDTO a partir de una entidad City
     * @param city Tipo de entidad desde la cual se realiza el mapeo
     * @return Nuevo objeto de tipo CityDTO
     */
    public CityDTO mapToCityDTO(City city) {
        return mapper.map(city, CityDTO.class);
    }
    /**
     * Mapear un TouristDTO a partir de una entidad Tourist
     * @param tourist Tipo de entidad desde la cual se realiza el mapeo
     * @return Nuevo objeto de tipo TouristDTO
     */
    public TouristDTO mapToTouristDTO(Tourist tourist) {
        return mapper.map(tourist, TouristDTO.class);
    }
    /**
     * Mapear un TripDTO a partir de una entidad Trip
     * @param trip Tipo de entidad desde la cual se realiza el mapeo
     * @return Nuevo Objeto de tipo TripDTO
     */
    public TripDTO mapToTripDTO(Trip trip) {
        return mapper.map(trip, TripDTO.class);
    }
}
