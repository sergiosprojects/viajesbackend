package com.example.viajesbacken.dto;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityDTO implements Serializable {

    private static final long serialVersionUID = 7098125792082986188L;

    private Long id;
    private String name;
    private int amountOfPeople;
    private String mostVisitedSite;
    private String mostReservedHotel;
}
