package com.example.viajesbacken.dto;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TouristDTO implements Serializable {

    private static final long serialVersionUID = 5449726483587198330L;

    private String id;
    private String names;
    private String surnames;
    private String idType;
    private LocalDate dateOfBirth;
    private int travelFrequency;
}
