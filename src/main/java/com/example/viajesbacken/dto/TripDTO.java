package com.example.viajesbacken.dto;

import com.example.viajesbacken.entity.City;
import com.example.viajesbacken.entity.Tourist;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TripDTO implements Serializable {

    private static final long serialVersionUID = 332714593568085194L;

    private Long id;
    private LocalDate date;
    private double budget;
    private boolean creditCard;
    private City city;
    private Tourist tourist;
}
