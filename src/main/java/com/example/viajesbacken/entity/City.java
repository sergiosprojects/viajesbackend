package com.example.viajesbacken.entity;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "CITIES")
public class City implements Serializable {

    private static final long serialVersionUID = -336014401279720916L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", length = 50, nullable = false)
    private String name;

    @Column(name = "AMOUNT_OF_PEOPLE", nullable = false)
    private int amountOfPeople;

    @Column(name = "MOST_VISITED_SITE", length = 50, nullable = false)
    private String mostVisitedSite;

    @Column(name = "MOST_RESERVED_HOTEL", length = 50, nullable = false)
    private String mostReservedHotel;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        City city = (City) o;
        return id != null && Objects.equals(id, city.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
