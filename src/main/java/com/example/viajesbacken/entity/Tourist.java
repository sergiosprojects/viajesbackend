package com.example.viajesbacken.entity;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "TOURISTS")
public class Tourist implements Serializable {

    private static final long serialVersionUID = 1074265771367965661L;

    @Id
    private String id;

    @Column(name = "NAMES", length = 70, nullable = false)
    private String names;

    @Column(name = "SURNAMES", length = 70, nullable = false)
    private String surnames;

    @Column(name = "ID_TYPE", length = 50, nullable = false)
    private String idType;

    @Column(name = "DATE_OF_BIRTH", nullable = false)
    private LocalDate dateOfBirth;

    @Column(name = "TRAVEL_FREQUENCY", nullable = false)
    private int travelFrequency;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Tourist tourist = (Tourist) o;
        return id != null && Objects.equals(id, tourist.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
