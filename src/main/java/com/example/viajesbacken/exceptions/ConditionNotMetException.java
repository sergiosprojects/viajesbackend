package com.example.viajesbacken.exceptions;

import java.io.Serializable;

/**
 * Excepción personalizada que se envía, cuando no se cumple una condición
 */
public class ConditionNotMetException extends Exception implements Serializable {

    private static final long serialVersionUID = -4603277832154138402L;

    public ConditionNotMetException(String message) {
        super(message);
    }

    public ConditionNotMetException(String message, Throwable cause) {
        super(message, cause);
    }
}
