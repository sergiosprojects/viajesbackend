package com.example.viajesbacken.exceptions;

import java.io.Serializable;

/**
 * Excepción personalizada que se envía cuando se desea guardar un registro existente como uno nuevo
 */
public class EntityByIdAlreadyExistException extends Exception implements Serializable {

    private static final long serialVersionUID = 4877363458405272852L;

    public EntityByIdAlreadyExistException(String message) {
        super(message);
    }

    public EntityByIdAlreadyExistException(String message, Throwable cause) {
        super(message, cause);
    }
}
