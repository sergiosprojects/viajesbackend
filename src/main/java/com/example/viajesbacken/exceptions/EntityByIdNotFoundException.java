package com.example.viajesbacken.exceptions;

import java.io.Serializable;

/**
 * Excepción personalizada que se envía cuando se desea actualizar un registro que no existe
 */
public class EntityByIdNotFoundException extends Exception implements Serializable {

    private static final long serialVersionUID = -6045326579934323367L;

    public EntityByIdNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityByIdNotFoundException(String message) {
        super(message);
    }
}
