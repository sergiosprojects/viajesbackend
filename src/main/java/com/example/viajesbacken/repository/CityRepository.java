package com.example.viajesbacken.repository;

import com.example.viajesbacken.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repositorio encargado de la comunicación con la tabla cities en la base de datos
 */
@Repository
public interface CityRepository extends JpaRepository<City, Long> {
}
