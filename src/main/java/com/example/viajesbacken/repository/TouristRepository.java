package com.example.viajesbacken.repository;

import com.example.viajesbacken.entity.Tourist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repositorio encargado de la comunicación con la tabla tourists en la base de datos
 */
@Repository
public interface TouristRepository extends JpaRepository<Tourist, String> {
}
