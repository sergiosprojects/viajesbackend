package com.example.viajesbacken.repository;

import com.example.viajesbacken.entity.City;
import com.example.viajesbacken.entity.Tourist;
import com.example.viajesbacken.entity.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
/**
 * Repositorio encargado de la comunicación con la tabla trips en la base de datos
 */
@Repository
public interface TripRepository extends JpaRepository<Trip, Long> {

    /**
     * Obtiene una lista de todos viajes filtrando por un turista específico
     * @param tourist Representa el turista por el cual se realiza el filtro
     * @return La lista de viajes encontrados
     */
    List<Trip> findAllByTourist(Tourist tourist);

    /**
     * Obtiene una lista de todos los viajes filtrando por una ciudad específica
     * @param city Representa la ciudad por la cual se realiza el filtro
     * @return La lista de viajes encontrados
     */
    List<Trip> findAllByCity(City city);

    /**
     * Obtiene una lista de todos los viajes filtrando por una fecha y una ciudad especificas
     * @param date Representa la fecha por la cual se desea filtrar
     * @param city Representa la ciudad por la cual se desea filtrar
     * @return La lista de viajes encontrados
     */
    List<Trip> findAllByDateAndCity(LocalDate date, City city);

    /**
     * Obtiene una lista de todos los viajes filtrando por una ciudad y un turista específicos
     * @param city Representa la ciudad por la cual se desea filtrar
     * @param tourist Representa el turista por el cual desea filtrar
     * @return La lista de viajes encontrados
     */
    List<Trip> findAllByCityAndTourist(City city, Tourist tourist);

}
