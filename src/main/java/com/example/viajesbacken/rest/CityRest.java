package com.example.viajesbacken.rest;

import com.example.viajesbacken.constants.ErrorMessageConstants;
import com.example.viajesbacken.converters.SingleModelMapper;
import com.example.viajesbacken.dto.CityDTO;
import com.example.viajesbacken.exceptions.EntityByIdAlreadyExistException;
import com.example.viajesbacken.exceptions.EntityByIdNotFoundException;
import com.example.viajesbacken.service.CityService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;


/**
 * Controlador de ciudad que se encarga de todas las peticiones HTTP para las operaciones CRUD
 */
@RestController
@RequestMapping("ciudades")
public class CityRest {

    private static final Log LOG = LogFactory.getLog(CityRest.class);

    private final CityService cityService;

    private final SingleModelMapper modelMapper;

    @Autowired
    public CityRest(CityService cityService, SingleModelMapper modelMapper) {
        this.cityService = cityService;
        this.modelMapper = modelMapper;
    }

    /**
     * Obtener todos los registros de ciudad haciendo uso del servicio
     * @return Lista DTO de las ciudades encontradas o una excepción en caso de que se genere algún error interno
     */
    @GetMapping
    public List<CityDTO> getAll() {
        try {
            return cityService.findAll().stream().map(modelMapper::mapToCityDTO)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.GET_ALL + e.getMessage(), e);
            return new ArrayList<>();
        }
    }

    /**
     * Obtiene una ciudad a partir de su ID haciendo uso del servicio y del Model Mapper
     * para el mapeo de Entidad a DTO
     *
     * @param id Representa el ID a partir del cual se realizará la búsqueda
     * @return Respuesta HTTP con status 200 (OK) que contiene en su cuerpo la ciudad DTO encontrada
     *         o un Not Found en caso de recibir una excepción de tipo NoSuchElementException
     *         o un Internal Server Error en caso de que algo más falle
     */
    @GetMapping("/{id}")
    public ResponseEntity<CityDTO> getById(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(modelMapper.mapToCityDTO(cityService.findById(id)));
        } catch (NoSuchElementException nsee) {
            LOG.error(ErrorMessageConstants.GET_BY_ID + nsee.getMessage(), nsee);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.GET_BY_ID + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Guarda una nueva ciudad, haciendo uso del servicio y del Model Mapper para el mapeo
     * de Entidad a DTO y viceversa
     *
     * @param cityDTO Representa la ciudad DTO que será mapeada a Entidad Ciudad y posteriormente guardada
     * @return Respuesta HTTP con status 201 (Created) que contiene en su cuerpo la ciudad DTO que se guardó
     *         o un Bad Request en caso de recibir una excepción de tipo EntityByIdAlreadyExistException
     *         o un Internal Server Error en caso de que algo más falle
     */
    @PostMapping
    public ResponseEntity<CityDTO> save(@RequestBody CityDTO cityDTO) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(modelMapper.mapToCityDTO(cityService.save(modelMapper.mapToCity(cityDTO))));
        } catch (EntityByIdAlreadyExistException ebiaee) {
            LOG.error(ErrorMessageConstants.SAVE + ebiaee.getMessage(), ebiaee);
            return ResponseEntity.badRequest().build();
        } catch (Exception e) {
            LOG.error(ErrorMessageConstants.SAVE + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Elimina una ciudad a partir de su ID haciendo uso del servicio
     * @param id Representa el ID de la ciudad que se desea eliminar
     * @return Respuesta HTTP con status 200 (OK)
     *         o un Not Found en caso de recibir una excepción de tipo IllegalArgumentException
     *         que indica que el ID es null
     *         o un Internal Server Error en caso de que algo más falle
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        try {
            cityService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (IllegalArgumentException iae) {
            LOG.error(ErrorMessageConstants.DELETE + iae.getMessage(), iae);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.DELETE + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Actualiza una ciudad existente haciendo uso del servicio y el Model Mapper para
     * el mapeo de Entidad a DTO y viceversa
     *
     * @param cityDTO Representa la ciudad DTO con los datos que se desean modificar, la cual
     *                será mapeada a entidad para ser actualizada
     * @return Respuesta HTTP con status 200 (OK)
     *         o un Not Found en caso de recibir una excepción de tipo EntityByIdNotFoundException
     *         o un Internal Server Error en caso de que algo mas falle
     */
    @PutMapping("/update")
    public ResponseEntity<String> update(@RequestBody CityDTO cityDTO) {
        try {
            cityService.update(modelMapper.mapToCity(cityDTO));
            return ResponseEntity.ok().build();
        } catch (EntityByIdNotFoundException ebinfe) {
            LOG.error(ErrorMessageConstants.UPDATE + ebinfe.getMessage(), ebinfe);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.UPDATE + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

}