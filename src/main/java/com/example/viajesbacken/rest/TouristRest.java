package com.example.viajesbacken.rest;

import com.example.viajesbacken.constants.ErrorMessageConstants;
import com.example.viajesbacken.converters.SingleModelMapper;
import com.example.viajesbacken.dto.TouristDTO;
import com.example.viajesbacken.exceptions.EntityByIdAlreadyExistException;
import com.example.viajesbacken.exceptions.EntityByIdNotFoundException;
import com.example.viajesbacken.service.TouristService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * Controlador de turista que se encarga de todas las peticiones HTTP para las operaciones CRUD
 */
@RestController
@RequestMapping("turistas")
public class TouristRest {

    private static final Log LOG = LogFactory.getLog(TouristRest.class);

    private final TouristService touristService;

    @Qualifier("ModelMapper")
    private final SingleModelMapper modelMapper;

    @Autowired
    public TouristRest(TouristService touristService, SingleModelMapper modelMapper) {
        this.touristService = touristService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<TouristDTO> getAll() {
        try {
            return touristService.findAll().stream().map(modelMapper::mapToTouristDTO)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.GET_ALL + e.getMessage(), e);
            return new ArrayList<>();
        }
    }

    /**
     * Obtiene un turista a partir de su ID haciendo uso del servicio y del Model Mapper
     * para el mapeo de Entidad a DTO
     *
     * @param id Representa el ID a partir del cual se realizará la búsqueda
     * @return Respuesta HTTP con status 200 (OK) que contiene en su cuerpo el viaje DTO encontrado
     *         o un Not Found en caso de recibir una excepción de tipo NoSuchElementException
     *         o un Internal Server Error en caso de que algo más falle
     */
    @GetMapping("/{id}")
    public ResponseEntity<TouristDTO> getById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(modelMapper.mapToTouristDTO(touristService.findById(id)));
        } catch (NoSuchElementException nsee) {
            LOG.error(ErrorMessageConstants.GET_BY_ID + nsee.getMessage(), nsee);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.GET_BY_ID + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Guarda una nueva ciudad haciendo uso del servicio y del Model Mapper para el mapeo
     * de Entidad a DTO y viceversa
     *
     * @param touristDTO Representa el turista DTO que será mapeado a Entidad Turista y posteriormente guardado
     * @return Respuesta HTTP con status 201 (Created) que contiene en su cuerpo el turista DTO que se guardó
     *         o un Bad Request en caso de recibir una excepción de tipo EntityByIdAlreadyExistException
     *         o un Internal Server Error en caso de que algo más falle
     */
    @PostMapping
    public ResponseEntity<TouristDTO> save(@RequestBody TouristDTO touristDTO) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(modelMapper.mapToTouristDTO(touristService.save(modelMapper.mapToTourist(touristDTO))));
        }
        catch (EntityByIdAlreadyExistException ebiaee) {
            LOG.error(ErrorMessageConstants.SAVE + ebiaee.getMessage(), ebiaee);
            return ResponseEntity.badRequest().build();
        }
        catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.SAVE + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Elimina un turista a partir de su ID haciendo uso del servicio
     * @param id Representa el ID el turista que se desea eliminar
     * @return Respuesta HTTP con status 200 (OK)
     *         o un Not Found en caso de recibir una excepción de tipo IllegalArgumentException
     *         que indica que el ID es null
     *         o un Internal Server Error en caso de que algo más falle
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable String id) {
        try {
            touristService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (IllegalArgumentException iae) {
            LOG.error(ErrorMessageConstants.DELETE + iae.getMessage(), iae);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.DELETE + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }


    /**
     * Actualiza un turista existente haciendo uso del servicio y el Model Mapper para
     * el mapeo de Entidad a DTO y viceversa
     *
     * @param touristDTO Representa el turista DTO con los datos que se desean modificar, el cual
     *                será mapeado a entidad para ser actualizado
     * @return Respuesta HTTP con status 200 (OK)
     *         o un Not Found en caso de recibir una excepción de tipo EntityByIdNotFoundException
     *         o un Internal Server Error en caso de que algo mas falle
     */
    @PutMapping("/update")
    public ResponseEntity<String> update(@RequestBody TouristDTO touristDTO) {
        try {
            touristService.update(modelMapper.mapToTourist(touristDTO));
            return ResponseEntity.ok().build();
        } catch (EntityByIdNotFoundException ebinfe) {
            LOG.error(ErrorMessageConstants.UPDATE + ebinfe.getMessage(), ebinfe);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.UPDATE + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }
}
