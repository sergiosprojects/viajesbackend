package com.example.viajesbacken.rest;

import com.example.viajesbacken.constants.ErrorMessageConstants;
import com.example.viajesbacken.converters.SingleModelMapper;
import com.example.viajesbacken.dto.TripDTO;
import com.example.viajesbacken.exceptions.ConditionNotMetException;
import com.example.viajesbacken.exceptions.EntityByIdAlreadyExistException;
import com.example.viajesbacken.exceptions.EntityByIdNotFoundException;
import com.example.viajesbacken.service.TripService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * Controlador de viaje que se encarga de todas las peticiones HTTP para las operaciones CRUD
 */
@RestController
@RequestMapping("viajes")
public class TripRest {

    private static final Log LOG = LogFactory.getLog(TripRest.class);

    private final TripService tripService;

    private final SingleModelMapper modelMapper;

    @Autowired
    public TripRest(TripService tripService, SingleModelMapper modelMapper) {
        this.tripService = tripService;
        this.modelMapper = modelMapper;
    }

    /**
     * Obtener todos los registros de viaje haciendo uso del servicio
     * @return Lista DTO de los viajes encontrados o una excepción en caso de que se genere algún error interno
     */
    @GetMapping
    public List<TripDTO> getAll(){
        try {
            return tripService.findAll().stream().map(modelMapper::mapToTripDTO)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.GET_ALL + e.getMessage(), e);
            return new ArrayList<>();
        }
    }

    /**
     * Obtiene un viaje a partir de su ID haciendo uso del servicio y del Model Mapper
     * para el mapeo de Entidad a DTO
     *
     * @param id Representa el ID a partir del cual se realizará la búsqueda
     * @return Respuesta HTTP con status 200 (OK) que contiene en su cuerpo el turista DTO encontrado
     *         o un Not Found en caso de recibir una excepción de tipo NoSuchElementException
     *         o un Internal Server Error en caso de que algo más falle
     */
    @GetMapping("/{id}")
    public ResponseEntity<TripDTO> getById(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(modelMapper.mapToTripDTO(tripService.findById(id)));
        } catch (NoSuchElementException nsee){
            LOG.error(ErrorMessageConstants.GET_BY_ID + nsee.getMessage(), nsee);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.GET_BY_ID + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Obtiene todos los registros de viaje filtrando por un turista determinado, haciendo uso
     * del servicio y del Model Mapper para el mapeo de Entidad a DTO
     *
     * @param touristId Representa el ID del turista por el cual se desea filtrar
     * @return Lista DTO de los viajes encontrados o una excepción en caso de que se genere algún error interno
     */
    @GetMapping("/turista/{id}")
    public List<TripDTO> getAllByTourist(@PathVariable (value = "id") String touristId){
        try {
            return tripService.findAllByTourist(touristId).stream().map(modelMapper::mapToTripDTO)
                    .collect(Collectors.toList());
        } catch (Exception e){
            LOG.fatal(ErrorMessageConstants.GET_ALL_BY_TOURIST + e.getMessage(), e);
            return new ArrayList<>();
        }
    }

    /**
     * Obtiene todos los registros de viaje filtrando por una ciudad determinada, haciendo uso
     * del servicio y del Model Mapper para el mapeo de Entidad a DTO
     *
     * @param cityId Representa el ID de la ciudad por el cual se desea filtrar
     * @return Lista DTO de los viajes encontrados o una excepción en caso de que se genere algún error interno
     */
    @GetMapping("/ciudad/{id}")
    public List<TripDTO> getAllByCity(@PathVariable (value = "id") Long cityId) {
       try {
           return tripService.findAllByCity(cityId).stream().map(modelMapper::mapToTripDTO)
                   .collect(Collectors.toList());
       } catch (Exception e){
           LOG.fatal(ErrorMessageConstants.GET_ALL_BY_CITY + e.getMessage(), e);
           return new ArrayList<>();
       }
    }

    /**
     * Guarda un nuevo turista, haciendo uso del servicio y del Model Mapper para el mapeo
     * de Entidad a DTO y viceversa
     *
     * @param tripDTO Representa el viaje DTO que será mapeado a Entidad Viaje y posteriormente guardado
     * @return Respuesta HTTP con status 201 (Created) que contiene en su cuerpo el viaje DTO que se guardó
     *         o un Bad Request en caso de recibir una excepción de tipo EntityByIdAlreadyExistException
     *         o un Internal Server Error en caso de que algo más falle
     */
    @PostMapping
    public ResponseEntity<TripDTO> save(@RequestBody TripDTO tripDTO) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(modelMapper.mapToTripDTO(tripService.save(modelMapper.mapToTrip(tripDTO))));
        } catch (ConditionNotMetException cnme) {
            LOG.error(ErrorMessageConstants.SAVE + cnme.getMessage(), cnme);
            return ResponseEntity.status(405).build();
        } catch (EntityByIdAlreadyExistException ebiae) {
            LOG.error(ErrorMessageConstants.SAVE + ebiae.getMessage(), ebiae);
            return ResponseEntity.badRequest().build();
        }
        catch (Exception e) {
            LOG.error(ErrorMessageConstants.SAVE + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Elimina un viaje a partir de su ID haciendo uso del servicio
     * @param id Representa el ID el viaje que se desea eliminar
     * @return Respuesta HTTP con status 200 (OK)
     *         o un Not Found en caso de recibir una excepción de tipo IllegalArgumentException
     *         que indica que el ID es null
     *         o un Internal Server Error en caso de que algo más falle
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        try {
            tripService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (IllegalArgumentException iae) {
            LOG.error(ErrorMessageConstants.DELETE + iae.getMessage(), iae);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.DELETE + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Actualiza un turista existente haciendo uso del servicio y el Model Mapper para
     * el mapeo de Entidad a DTO y viceversa
     *
     * @param tripDTO Representa el viaje DTO con los datos que se desean modificar, el cual
     *                será mapeado a entidad para ser actualizado
     * @return Respuesta HTTP con status 200 (OK)
     *         o un Not Found en caso de recibir una excepción de tipo EntityByIdNotFoundException
     *         o un Internal Server Error en caso de que algo mas falle
     */
    @PutMapping("/update")
    public ResponseEntity<String> update(@RequestBody TripDTO tripDTO) {
        try {
            tripService.update(modelMapper.mapToTrip(tripDTO));
            return ResponseEntity.ok().build();
        } catch (EntityByIdNotFoundException ebinfe) {
            LOG.error(ErrorMessageConstants.UPDATE + ebinfe.getMessage(), ebinfe);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            LOG.fatal(ErrorMessageConstants.UPDATE + e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }
}
