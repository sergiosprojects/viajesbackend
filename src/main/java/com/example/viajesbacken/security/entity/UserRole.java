package com.example.viajesbacken.security.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "USER_ROLES")
public class UserRole implements Serializable {

    private static final long serialVersionUID = 5789891729742905548L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ROLE_ID", nullable = false)
    private Long userRoleID;

    @Column(name = "ROLE", nullable = false, length = 50)
    private String role;

    @OneToMany(mappedBy = "userRole")
    @ToString.Exclude
    private Set<Usr>  user = new HashSet<>();
}
