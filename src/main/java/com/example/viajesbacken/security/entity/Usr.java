package com.example.viajesbacken.security.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "USERS")
public class Usr implements Serializable {

    @Id
    @Column(name = "USERNAME", nullable = false, length = 45)
    private String username;

    @Column(name = "PASSWORD", nullable = false, length = 60)
    private String password;

    @ManyToOne
    @JoinColumn(name = "USER_ROLE_ID")
    private UserRole userRole;
}
