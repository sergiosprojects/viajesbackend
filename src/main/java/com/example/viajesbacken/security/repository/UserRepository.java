package com.example.viajesbacken.security.repository;

import com.example.viajesbacken.security.entity.Usr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Usr, String> {
    Usr findByUsername(String username);
}
