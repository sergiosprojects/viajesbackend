package com.example.viajesbacken.security.repository;

import com.example.viajesbacken.security.entity.UserRole;
import com.example.viajesbacken.security.entity.Usr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    UserRole findByUser(Usr user);
}
