package com.example.viajesbacken.security.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("auth")
public class AuthRest {

    @GetMapping(path = "/basicauth")
    public boolean Login() {
        return true;
    }
}
