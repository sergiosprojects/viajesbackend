package com.example.viajesbacken.service;

import com.example.viajesbacken.entity.City;
import com.example.viajesbacken.exceptions.EntityByIdAlreadyExistException;
import com.example.viajesbacken.exceptions.EntityByIdNotFoundException;

import java.util.List;

/**
 * Servicio de ciudad que contiene la lógica del negocio, con todas las operaciones CRUD
 */
public interface CityService {

    /**
     * Obtiene una ciudad a partir de su id
     * @param id Representa el ID a partir del cual se realizará la búsqueda
     * @return La ciudad encontrada por ID o genera una excepción de tipo NoSuchElementException en caso de no
     *         encontrar alguna
     */
    City findById(Long id);

    /**
     * Obtiene una lista de todas las ciudades existentes
     * @return Lista de ciudades encontradas
     */
    List<City> findAll();

    /**
     * Guarda una ciudad nueva
     * @param city Representa la ciudad que se desea guardar
     * @return La ciudad que se guardó
     * @throws EntityByIdAlreadyExistException Excepción personalizada en caso de que la ciudad ya exista
     */
    City save(City city) throws EntityByIdAlreadyExistException;

    /**
     * Actualizar una ciudad existente
     * @param city Representa la ciudad con el ID que se desea actualizar, con los demás datos modificados
     * @throws EntityByIdNotFoundException Excepción personalizada en caso de que no exista la ciudad
     */
    void update(City city) throws EntityByIdNotFoundException;

    /**
     * Elimina una ciudad por su ID, en caso de que no exista la ciudad,
     * Lanza una excepción de tipo IllegalArgumentException
     * @param id Representa el ID de ciudad que se desea eliminar
     */
    void deleteById(Long id);
}
