package com.example.viajesbacken.service;

import com.example.viajesbacken.entity.City;
import com.example.viajesbacken.exceptions.EntityByIdAlreadyExistException;
import com.example.viajesbacken.exceptions.EntityByIdNotFoundException;
import com.example.viajesbacken.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public City findById(Long id) {
        return cityRepository.findById(id).orElseThrow();
    }

    @Override
    @Transactional(readOnly = true)
    public List<City> findAll() {
        return cityRepository.findAll();
    }

    @Override
    @Transactional
    public City save(City city) throws EntityByIdAlreadyExistException {
        if (city.getId() != null && cityRepository.existsById(city.getId())) {
            throw new EntityByIdAlreadyExistException("Ya existe una ciudad con el ID "
                    + city.getId());
        }
        return cityRepository.save(city);
    }

    @Override
    @Transactional
    public void update(City city) throws EntityByIdNotFoundException {
        if (!cityRepository.existsById(city.getId())) {
            throw new EntityByIdNotFoundException("No existe la ciudad con el ID "
                    + city.getId());
        }
        cityRepository.save(city);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        cityRepository.deleteById(id);
    }
}
