package com.example.viajesbacken.service;

import com.example.viajesbacken.entity.Tourist;
import com.example.viajesbacken.exceptions.EntityByIdAlreadyExistException;
import com.example.viajesbacken.exceptions.EntityByIdNotFoundException;

import java.util.List;

/**
 * Servicio de turista que contiene la lógica del negocio, con todas las operaciones CRUD
 */
public interface TouristService {

    /**
     * Obtiene un turista a partir de su id
     * @param id Representa el ID a partir del cual se realizará la búsqueda
     * @return El turista encontrado por ID o genera una excepción de tipo NoSuchElementException en caso de no
     *         encontrar alguno
     */
    Tourist findById(String id);

    /**
     * Obtiene una lista de todos los turistas existentes
     * @return Lista de turistas encontrados
     */
    List<Tourist> findAll();

    /**
     * Guarda un turista nuevo
     * @param tourist Representa El turista que se desea guardar
     * @return El turista que se guardó
     * @throws EntityByIdAlreadyExistException Excepción personalizada en caso de que el turista ya exista
     */
    Tourist save(Tourist tourist) throws EntityByIdAlreadyExistException;

    /**
     * Actualizar una ciudad existente
     * @param tourist Representa el turista con el ID que se desea actualizar, con los demás datos modificados
     * @throws EntityByIdNotFoundException Excepción personalizada en caso de que no exista el turista
     */
    void update(Tourist tourist) throws EntityByIdNotFoundException;

    /**
     * Elimina un turista por su ID, en caso de que no exista el turista,
     * Lanza una excepción de tipo IllegalArgumentException
     * @param id Representa el ID del turista que se desea eliminar
     */
    void deleteById(String id);
}
