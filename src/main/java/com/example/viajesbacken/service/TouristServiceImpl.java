package com.example.viajesbacken.service;

import com.example.viajesbacken.entity.Tourist;
import com.example.viajesbacken.exceptions.EntityByIdAlreadyExistException;
import com.example.viajesbacken.exceptions.EntityByIdNotFoundException;
import com.example.viajesbacken.repository.TouristRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TouristServiceImpl implements TouristService {

    private final TouristRepository touristRepository;

    @Autowired
    public TouristServiceImpl(TouristRepository touristRepository){
        this.touristRepository = touristRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Tourist findById(String id) {
        return touristRepository.findById(id).orElseThrow();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Tourist> findAll() {
        return touristRepository.findAll();
    }

    @Override
    @Transactional
    public Tourist save(Tourist tourist) throws EntityByIdAlreadyExistException {
        if (tourist.getId() != null && touristRepository.existsById(tourist.getId())) {
            throw new EntityByIdAlreadyExistException("Ya existe un turista con el ID: "
                    + tourist.getId());
        }
        return touristRepository.save(tourist);
    }

    @Override
    @Transactional
    public void update(Tourist tourist) throws EntityByIdNotFoundException {
        if (!touristRepository.existsById(tourist.getId())) {
            throw new EntityByIdNotFoundException("No existe un turista con el ID: "
                    + tourist.getId());
        }
        touristRepository.save(tourist);
    }


    @Override
    @Transactional
    public void deleteById(String id) {
        touristRepository.deleteById(id);
    }
}
