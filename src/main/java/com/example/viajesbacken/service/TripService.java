package com.example.viajesbacken.service;

import com.example.viajesbacken.entity.Trip;
import com.example.viajesbacken.exceptions.ConditionNotMetException;
import com.example.viajesbacken.exceptions.EntityByIdAlreadyExistException;
import com.example.viajesbacken.exceptions.EntityByIdNotFoundException;

import java.util.List;

/**
 * Servicio de viaje que contiene la lógica del negocio, con todas las operaciones CRUD
 * y las validaciones para permitir guardar un viaje
 */
public interface TripService {

    /**
     * Obtiene una lista de todos los viajes existentes
     * @return Lista de viajes encontrados
     */
    List<Trip> findAll();

    /**
     * Obtiene un viaje a partir de su ID
     * @param id Representa el ID a partir del cual se va a realizar la búsqueda
     * @return El Trip encontrado por ID o genera una excepción de tipo NoSuchElementException en caso de no
     *         encontrar alguno
     */
    Trip findById(Long id);

    /**
     * Obtiene una lista de todos los viajes existentes filtrando por un turista específico
     * @param touristId Representa el ID del turista que será capturado mediante el servicio de turista.
     *                  Posteriormente se realiza la consulta
     * @return Lista de viajes encontrados
     */
    List<Trip> findAllByTourist(String touristId);

    /**
     * Obtiene una lista de todos los viajes existentes filtrando por una ciudad específica
     * @param cityId Representa el ID de la ciudad que será capturada mediante el servicio de ciudad.
     *               Posteriormente se realiza la consulta
     * @return Lista de viajes encontrados
     */
    List<Trip> findAllByCity(Long cityId);

    /**
     * Guarda un viaje nuevo
     * @param trip Representa el viaje que se desea guardar
     * @return El viaje que se guardó
     * @throws EntityByIdAlreadyExistException Excepción personalizada en caso de que el viaje ya exista
     * @throws ConditionNotMetException Excepción personalizada en caso de que no se cumplan las condiciones del negocio
     */
    Trip save(Trip trip) throws EntityByIdAlreadyExistException, ConditionNotMetException;

    /**
     * Actualiza un viaje existente
     * @param trip Representa el viaje con el ID que se desea actualizar, con los demás datos modificados
     * @throws EntityByIdNotFoundException Excepción en caso de que no exista el viaje
     * @throws ConditionNotMetException Excepción en caso de que no se cumplan las condiciones del negocio
     */
    void update(Trip trip) throws EntityByIdNotFoundException, ConditionNotMetException;

    /**
     * Elimina un viaje por su ID, en caso de que no exista el viaje,
     * Lanza una excepción de tipo IllegalArgumentException
     * @param id Representa el ID del viaje que se desea eliminar
     */
    void deleteById(Long id);

    /**
     * Válida si ya existen 5 viajes para la misma ciudad, en la misma fecha
     * @param trip Representa el viaje del cual se extrae la ciudad y la fecha para la validación
     * @return Verdadero: en caso de que existan 5 viajes o más
     *         Falso: en caso de que existan 4 viajes o menos
     */
    boolean fiveTripsToTheCityToDate(Trip trip);

    /**
     * Válida si ya existe 1 viaje del mismo turista, para la misma ciudad, en la misma fecha
     * @param trip Representa el viaje del cual se extrae la ciudad y la fecha, para la validación
     * @return Verdadero: en caso de que ya exista 1 viaje o más
     *         Falso: en caso de que no existan viajes
     */
    boolean oneTripOnDayForCityAndTourist(Trip trip);
}
