package com.example.viajesbacken.service;

import com.example.viajesbacken.entity.City;
import com.example.viajesbacken.entity.Tourist;
import com.example.viajesbacken.entity.Trip;
import com.example.viajesbacken.exceptions.ConditionNotMetException;
import com.example.viajesbacken.exceptions.EntityByIdAlreadyExistException;
import com.example.viajesbacken.exceptions.EntityByIdNotFoundException;
import com.example.viajesbacken.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TripServiceImpl implements TripService {

    private final TripRepository tripRepository;

    private final TouristService touristService;

    private final CityService cityService;

    @Autowired
    public TripServiceImpl( TripRepository tripRepository,
                            TouristService touristService,
                            CityService cityService) {
        this.tripRepository = tripRepository;
        this.touristService = touristService;
        this.cityService = cityService;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Trip> findAll() {
        return tripRepository.findAll();
    }

    @Override
    public Trip findById(Long id) {
        return tripRepository.findById(id).orElseThrow();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Trip> findAllByTourist(String touristId) {
        Tourist tourist = touristService.findById(touristId);
        return tripRepository.findAllByTourist(tourist);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Trip> findAllByCity(Long cityId) {
        City city = cityService.findById(cityId);
        return tripRepository.findAllByCity(city);
    }

    @Override
    @Transactional
    public Trip save(Trip trip) throws EntityByIdAlreadyExistException, ConditionNotMetException {
        if (fiveTripsToTheCityToDate(trip) || oneTripOnDayForCityAndTourist(trip)) {
            throw new ConditionNotMetException("Se ha incumplido alguna condición");
        }
        if (trip.getId() != null && tripRepository.existsById(trip.getId())) {
            throw new EntityByIdAlreadyExistException("Ya existe un viaje con el ID: "
                    + trip.getId());
        }
        return tripRepository.save(trip);
    }

    @Override
    public void update(Trip trip) throws EntityByIdNotFoundException, ConditionNotMetException {
        if (!tripRepository.existsById(trip.getId())) {
            throw new EntityByIdNotFoundException("No existe un viaje con el ID: "
                    + trip.getId());
        }
        tripRepository.save(trip);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        tripRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean fiveTripsToTheCityToDate(Trip trip) {
        return tripRepository.findAllByDateAndCity(
                trip.getDate(), trip.getCity()
        ).size() >= 5;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean oneTripOnDayForCityAndTourist(Trip trip) {
        List<Trip> trips = tripRepository.findAllByCityAndTourist(
                trip.getCity(), trip.getTourist()
        );

        return !trips.stream()
                .filter(v -> v.getDate().getMonth().equals(trip.getDate().getMonth()))
                .collect(Collectors.toList())
                .isEmpty();
    }
}
