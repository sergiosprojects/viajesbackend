package com.example.viajesbacken.rest;

import com.example.viajesbacken.constants.SqlStatements;
import com.example.viajesbacken.converters.SingleModelMapper;
import com.example.viajesbacken.dto.CityDTO;
import com.example.viajesbacken.entity.City;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CityRestTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private SingleModelMapper modelMapper;

    private static final String HOST = "http://localhost:";

    private final City city = new City();

    private final RowMapper<City> rm = BeanPropertyRowMapper.newInstance(City.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    void setUp() {
        city.setId(1L);
        city.setName("Cali");
        city.setAmountOfPeople(1000000);
        city.setMostReservedHotel("La perla");
        city.setMostVisitedSite("Parque explora");
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            scripts = {"classpath:city/insert.sql", "classpath:auth/validate_auth.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:city/delete.sql")
    void getAll() throws NullPointerException, URISyntaxException, DataAccessException {
        URI uri = new URI(HOST + port + "/ciudades");
        ParameterizedTypeReference<List<CityDTO>> entity = new ParameterizedTypeReference<>() {};
        ResponseEntity<List<CityDTO>> result = restTemplate
                .withBasicAuth("spring", "123")
                .exchange(uri, HttpMethod.GET, null, entity);

        List<City> cityList = jdbcTemplate.query(SqlStatements.SELECT_ALL_CITIES, rm);
        List<CityDTO> collection = new ArrayList<>();

        for (City c : cityList)
            collection.add(modelMapper.mapToCityDTO(c));

        assertEquals(200, result.getStatusCodeValue());
        assertTrue(Objects.requireNonNull(result.getBody()).containsAll(collection));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            scripts = {"classpath:city/insert_one.sql", "classpath:auth/validate_auth.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:city/delete_one.sql")
    void getById() throws NullPointerException, URISyntaxException, DataAccessException {
        URI uri = new URI(HOST + port + "/ciudades/" + city.getId());
        ResponseEntity<CityDTO> result = restTemplate
                .withBasicAuth("spring", "123")
                .getForEntity(uri, CityDTO.class);

        City found = jdbcTemplate.queryForObject(
                SqlStatements.SELECT_ALL_CITIES_BY_ID, rm, city.getId());

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(result.getBody(), modelMapper.mapToCityDTO(found));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            scripts = {"classpath:city/delete_by_name.sql", "classpath:auth/validate_auth.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:city/delete_by_name.sql")
    void save() throws NullPointerException, URISyntaxException, DataAccessException {
        URI uri = new URI(HOST + port + "/ciudades");
        HttpEntity<CityDTO> entity = new HttpEntity<>(modelMapper.mapToCityDTO(city));
        ResponseEntity<CityDTO> result = restTemplate
                .withBasicAuth("spring", "123")
                .postForEntity(uri, entity, CityDTO.class);

        City created = jdbcTemplate.queryForObject(
                SqlStatements.SELECT_ALL_CITIES_BY_NAME, rm, city.getName());

        assertEquals(201, result.getStatusCodeValue());
        assertEquals(result.getBody(), modelMapper.mapToCityDTO(created));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            scripts = {"classpath:city/insert_one.sql", "classpath:auth/validate_auth.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:city/delete_one.sql")
    void delete() throws NullPointerException, URISyntaxException, DataAccessException {
        URI uri = new URI(HOST + port + "/ciudades/delete/1");
        ResponseEntity<String> result = restTemplate
                .withBasicAuth("spring", "123")
                .exchange(uri, HttpMethod.DELETE, null, String.class);

        Integer count = jdbcTemplate.queryForObject(SqlStatements.COUNT_ALL_CITIES_BY_ID, Integer.class, city.getId());

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(0, count);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            scripts = {"classpath:city/insert_one.sql", "classpath:auth/validate_auth.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:city/delete_one.sql")
    void update() throws NullPointerException, URISyntaxException, DataAccessException {
        City updatedCity = city;
        updatedCity.setName("Armenia");
        updatedCity.setAmountOfPeople(2000000);

        URI uri = new URI(HOST + port + "/ciudades/update");
        HttpEntity<CityDTO> entity = new HttpEntity<>(modelMapper.mapToCityDTO(updatedCity));
        ResponseEntity<String> result = restTemplate
                .withBasicAuth("spring", "123")
                .exchange(uri, HttpMethod.PUT, entity, String.class);

        City updated = jdbcTemplate.queryForObject(
                SqlStatements.SELECT_ALL_CITIES_BY_ID, rm, city.getId());

        assertEquals(200, result.getStatusCodeValue(), "Http status");
        assertEquals(updated, updatedCity, "Igualdad entre la ciudad enviada y la ciudad consultada");
    }
}