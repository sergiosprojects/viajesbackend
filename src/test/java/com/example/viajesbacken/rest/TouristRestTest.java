package com.example.viajesbacken.rest;

import com.example.viajesbacken.constants.SqlStatements;
import com.example.viajesbacken.converters.SingleModelMapper;
import com.example.viajesbacken.dto.TouristDTO;
import com.example.viajesbacken.entity.Tourist;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class TouristRestTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private SingleModelMapper modelMapper;

    private static final String HOST = "http://localhost:";

    private final Tourist tourist = new Tourist();

    private final RowMapper<Tourist> rm = BeanPropertyRowMapper.newInstance(Tourist.class);

    @BeforeEach
    void setUp() {
        tourist.setId("123");
        tourist.setNames("Sergio");
        tourist.setSurnames("Garcia");
        tourist.setDateOfBirth(LocalDate.of(1998,3,16));
        tourist.setIdType("C.C");
        tourist.setTravelFrequency(2);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            scripts = {"classpath:tourist/insert.sql", "classpath:auth/validate_auth.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:tourist/delete.sql")
    void getAll() throws URISyntaxException {
        URI uri = new URI(HOST + port + "/turistas");
        ParameterizedTypeReference<List<TouristDTO>> entity = new ParameterizedTypeReference<>() {};
        ResponseEntity<List<TouristDTO>> result = restTemplate
                .withBasicAuth("spring", "123")
                .exchange(uri, HttpMethod.GET, null, entity);

        List<Tourist> touristList = jdbcTemplate.query(SqlStatements.SELECT_ALL_TOURISTS, rm);
        List<TouristDTO> collection = touristList.stream()
                                                 .map(modelMapper::mapToTouristDTO)
                                                 .collect(Collectors.toList());

        assertEquals(200, result.getStatusCodeValue());
        assertTrue(Objects.requireNonNull(result.getBody()).containsAll(collection), "");
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            scripts = {"classpath:tourist/insert_one.sql", "classpath:auth/validate_auth.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:tourist/delete_one.sql")
    void getById() throws URISyntaxException {
        URI uri = new URI(HOST + port + "/turistas/" + tourist.getId());
        ResponseEntity<TouristDTO> result = restTemplate
                .withBasicAuth("spring", "123")
                .getForEntity(uri, TouristDTO.class);

        Tourist found = jdbcTemplate.queryForObject(
                SqlStatements.SELECT_ALL_TOURISTS_BY_ID, rm, tourist.getId());

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(result.getBody(), modelMapper.mapToTouristDTO(found));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            scripts = {"classpath:tourist/delete_one.sql", "classpath:auth/validate_auth.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:tourist/delete_one.sql")
    void save() throws URISyntaxException {
        URI uri = new URI(HOST + port + "/turistas");
        HttpEntity<TouristDTO> entity = new HttpEntity<>(modelMapper.mapToTouristDTO(tourist));
        ResponseEntity<TouristDTO> result = restTemplate
                .withBasicAuth("spring", "123")
                .postForEntity(uri, entity, TouristDTO.class);

        Tourist created = jdbcTemplate.queryForObject(
                SqlStatements.SELECT_ALL_TOURISTS_BY_ID, rm, tourist.getId());

        assertEquals(201, result.getStatusCodeValue());
        assertEquals(result.getBody(), modelMapper.mapToTouristDTO(created));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            scripts = {"classpath:tourist/insert_one.sql", "classpath:auth/validate_auth.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:tourist/delete_one.sql")
    void delete() throws URISyntaxException {
        URI uri = new URI(HOST + port + "/turistas/delete/" + tourist.getId());
        ResponseEntity<String> result = restTemplate
                .withBasicAuth("spring", "123")
                .exchange(uri, HttpMethod.DELETE, null, String.class);

        Integer count = jdbcTemplate.queryForObject(
                SqlStatements.COUNT_ALL_TOURISTS_BY_ID, Integer.class, tourist.getId());

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(0, count);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            scripts = {"classpath:tourist/insert_one.sql", "classpath:auth/validate_auth.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:tourist/delete_one.sql")
    void update() throws URISyntaxException {
        Tourist updatedTourist = tourist;
        updatedTourist.setNames("Juan");
        updatedTourist.setDateOfBirth(LocalDate.of(2001, 11, 22));
        URI uri = new URI(HOST + port + "/turistas/update");
        HttpEntity<TouristDTO> entity = new HttpEntity<>(modelMapper.mapToTouristDTO(updatedTourist));
        ResponseEntity<String> result = restTemplate
                .withBasicAuth("spring", "123")
                .exchange(uri, HttpMethod.PUT, entity, String.class);

        Tourist updated = jdbcTemplate.queryForObject(
                SqlStatements.SELECT_ALL_TOURISTS_BY_ID, rm, tourist.getId());

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(updated, updatedTourist);
    }
}