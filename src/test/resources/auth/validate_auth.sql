SET IDENTITY_INSERT user_roles ON;
IF NOT EXISTS (SELECT * FROM user_roles WHERE user_role_id = 1 AND role = 'ADMIN')
    INSERT INTO user_roles (user_role_id, role) VALUES (1, 'ADMIN');
IF NOT EXISTS (SELECT * FROM users WHERE username = 'spring')
    INSERT INTO users (username, password, user_role_id) VALUES ('spring', '$2a$12$qyhdG1.X8TK0ghG/8tJ43udMP6TCjJXvMYyzYd82ClDQgNYLDYU9G', 1);
SET IDENTITY_INSERT user_roles OFF;