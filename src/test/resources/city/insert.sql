DELETE FROM cities WHERE id IN (1, 2);
SET IDENTITY_INSERT cities ON;
INSERT INTO cities (id, amount_of_people, most_reserved_hotel, most_visited_site, name)
VALUES (1, '1000000', 'La perla', 'Parque explora', N'Medellín'),
       (2, '123456789', 'Estrella', 'Skate Park', 'Cali');
SET IDENTITY_INSERT cities OFF;