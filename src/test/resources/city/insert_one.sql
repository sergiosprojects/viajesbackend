DELETE FROM cities WHERE id = 1;
SET IDENTITY_INSERT cities ON;
INSERT INTO cities (id, amount_of_people, most_reserved_hotel, most_visited_site, name)
VALUES (1, '1000000', 'La perla', 'Parque explora', 'Cali');
SET IDENTITY_INSERT cities OFF;