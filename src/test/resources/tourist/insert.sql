DELETE FROM tourists WHERE id IN ('123', '567');
INSERT INTO tourists (id, date_of_birth, id_type, names, surnames, travel_frequency)
VALUES ('123', '1998-03-16', 'C.C', 'Sergio', 'Garcia', '2'),
       ('567', '2000-11-25', 'T.I', 'Daniel', 'Correa', '6');